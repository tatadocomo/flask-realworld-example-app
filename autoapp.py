# -*- coding: utf-8 -*-
"""Create an application instance."""
import sys, os

sys.path.insert(1, os.path.abspath("conduit"))

from flask.helpers import get_debug_flag

from app import create_app
from settings import DevConfig, ProdConfig

CONFIG = DevConfig if get_debug_flag() else ProdConfig

app = create_app(CONFIG)