# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
import sys, os

sys.path.insert(1, os.path.abspath("conduit"))

import flask
import user
import extensions
import settings
import exceptions

from user import views as uviews
from user import models as umodels
from profile import views as pviews
from profile import models as umodels
from articles import views as aviews
from articles import models as amodels

from flask import Flask, render_template
from extensions import bcrypt, cache, db, migrate, jwt, cors

import commands, user, profile, articles, settings, exceptions
from settings import ProdConfig
from exceptions import InvalidUsage


def create_app(config_object=ProdConfig):
    """An application factory, as explained here:
    http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.url_map.strict_slashes = False
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    bcrypt.init_app(app)
    cache.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)


def register_blueprints(app):
    """Register Flask blueprints."""
    origins = app.config.get('CORS_ORIGIN_WHITELIST', '*')
    cors.init_app(uviews.blueprint, origins=origins)
    cors.init_app(pviews.blueprint, origins=origins)
    cors.init_app(aviews.blueprint, origins=origins)

    app.register_blueprint(uviews.blueprint)
    app.register_blueprint(pviews.blueprint)
    app.register_blueprint(aviews.blueprint)


def register_errorhandlers(app):

    def errorhandle(error):
        error_code = getattr(error, 'code', 500)
        return render_template(f"errors/{error_code}.html"), error_code
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(errorhandle)


def register_shellcontext(app):
    """Register shell context objects."""
    def shell_context():
        """Shell context objects."""
        return {
            'db': db,
            'User': umodels.User,
            'UserProfile': pmodels.UserProfile,
            'Article': amodels.Article,
            'Tag': amodels.Tags,
            'Comment': amodels.Comment,
        }

    app.shell_context_processor(shell_context)

def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.test)
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)