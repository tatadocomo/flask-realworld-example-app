# -*- coding: utf-8 -*-
import sys, os

sys.path.insert(1, os.path.abspath("conduit"))
# sys.path.insert(1, os.path.abspath("C:\\Users\\Tara Relan\\Documents\\GitHub\\flask-realworld-example-app\\conduit"))
import user
from user import *

"""Helper utilities and decorators."""
# from user.models import User  # noqa


def jwt_identity(payload):
    return User.get_by_id(payload)


def identity_loader(user):
    return user.id
